# README #


### Summary ###

This program is designed to solve the train shunting problem. For a full understanding of the problem please see [the writeup](https://www.kth.se/social/upload/5273e202f2765461e70bbe78/train.pdfL) from Christian Shulte.
V1.0

### Set-up ###

This program was developed using Gracket v6.4 but should work with earlier versions.
In order to run tests just run

    Racket tests.rkt


### Contact ###

Please contact alf40k@gmail.com or ArthurLLunn@gmail.com with any questions