#lang racket
;; file: find-fewest.rkt
;; author: Arthur Lunn | all3187
;; language: racket
;; purpose: Finds the fewest number of moves to chain one train into another.


(require "lists.rkt")
(require "find-moves.rkt")
(provide find-fewest)
; find-fewest : list list list list -> list
; Takes in a main train, one train,two train and a 'match' train then returns
; the moves required to change the main train into the 'match' train
(define (find-fewest Ms Os Ts Ys)
  (cond ((null? Ys) '())
        ((and (not (null? Ms ) ) (equal? (car Ms) (car Ys))) (find-fewest (cdr Ms) Os Ts (cdr Ys)))
        ((member? Ms (car Ys))
         (annex
          (list
           (list "one" (- (len Ms) (- (pos Ms (car Ys)) 1)) )
           (list "two" (- (pos Ms (car Ys)) 1)) 
           (list "one" (- 0 (- (len Ms) (- (pos Ms (car Ys)) 1)) ))
           )
          (find-fewest (if (null? (chop (split-train Ms (car Ys)) 1))
                           '()
                           (car (chop (split-train Ms (car Ys)) 1))
                      )
                      Os
                      (annex
                       (if (null? (car (split-train Ms (car Ys))))
                           '()
                           (car (split-train Ms (car Ys)))
                           )
                       Ts
                      )
                      (cdr Ys))
          )
         ) ;; Handle if next is in main but out of sequence.
        ((member? Os (car Ys))
         (annex
          (list
           (if ( equal? (- 0 (- (pos Os (car Ys)) 1)) 0 )
               '()
               (list "one" (- 0 (- (pos Os (car Ys)) 1)))
               )
           (if ( equal? (- (pos Os (car Ys)) 1) 0 )
               '()
               (list "two" (- (pos Os (car Ys)) 1))
               )           
           (list "one" -1)
           )
          (find-fewest Ms
                      (if (null? (chop (split-train Os (car Ys)) 1))
                                 '()
                                 (car (chop (split-train Os (car Ys)) 1))
                       )
                       (annex
                       (if (null? (car (split-train Os (car Ys))))
                           '()
                           (car (split-train Os (car Ys)))
                           )
                       Ts
                      )
                       (cdr Ys))
          )
         );; Handle if next is in Os
        ((member? Ts (car Ys))
         (annex
          (list
           (list "two" (- 0 (- (pos Ts (car Ys)) 1)))
           (list "one" (- (pos Ts (car Ys)) 1))
           (list "two" -1)
           )
          (find-fewest Ms
                      (annex
                       (if (null? (car (split-train Ts (car Ys))))
                           '()
                           (car (split-train Ts (car Ys)))
                           )
                       Os
                      )
                      (if (null? (chop (split-train Ts (car Ys)) 1))
                                 '()
                                 (car (chop (split-train Ts (car Ys)) 1))
                       )
                       (cdr Ys))
          )
         );; Handle if next is in Ts
  )
)





